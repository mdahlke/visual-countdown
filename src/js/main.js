wisnet.App.setProp('timerSeconds', 120);

$(function () {
	$('#timer-stop, #timer-reset').hide();
	
	const $timer = $('#timer');
	const animationDuration = 1000;
	const timerTrackSelectorID = 'timerTrack';
	const timerTrackSelector = '#' + timerTrackSelectorID;
	const $timerTrack = $(timerTrackSelector);
	let timerInterval;
	let timerAmount;
	let loading = true;
	let activeTimer = false;
	let secondsLeft = false;
	
	$timer.circleProgress({
		startAngle: -Math.PI / 2,
		reverse: true,
		value: 1,
		size: $timer.width(),
		thickness: $timer.width() / 2,
		fill: {
			gradient: ['red', 'orange']
		},
		animation: {
			duration: animationDuration,
			easing: 'linear'
		},
		animationStartValue: 1.0
	});
	
	
	if (!$timerTrack.length) {
		$('body').append($('<audio />', {
			'id': timerTrackSelectorID
		}).attr({
			'src': wisnet.App.getProp('timerDoneAudio'),
			preload: 'auto'
		}));
	}
	
	$('#timer-value').on('change', function () {
	
	});
	
	$('#timer-start').on('click', function () {
		// let seconds = $('#timer-value').val() * 60;
		let minutes = $('#timer-minutes').val();
		let seconds = parseInt($('#timer-seconds').val());
		
		$('#timer-stop').show();
		$('#timer-reset, #timer-start').hide();
		
		loading = true;
		
		let track = $(timerTrackSelector)[0];
		track.volume = 0;
		console.log(track.volume);
		track.play();
		
		seconds += minutes * 60;
		// to account for the duration we'll subtract the animation time
		seconds -= (animationDuration / 1000);
		
		wisnet.App.setProp('timerSeconds', seconds);
		
		startCountdown(secondsLeft ? secondsLeft : seconds).then(function (originalSeconds) {
			playBeeps(3);
			$('#timer-stop').hide();
			$('#timer-reset').show();
		});
	});
	
	$('#timer-stop').on('click', function () {
		$('#timer-start, #timer-reset').show();
		
		clearInterval(timerInterval);
	});
	
	$('#timer-reset').on('click', function () {
		$('#timer-start').show();
		$('#timer-stop, #timer-reset').hide();
		
		$timer.circleProgress('value', 1);
		$('#timer-left').text('');
		
		activeTimer = false;
		secondsLeft = false;
	});
	
	function startCountdown(seconds = false) {
		wisnet.App.setProp('timerOriginalSeconds', seconds);
		loading = false;
		
		activeTimer = true;
		
		return new Promise(function (resolve, reject) {
			
			clearInterval(timerInterval);
			if (seconds === false) {
				seconds = wisnet.App.getProp('timerSeconds');
			}
			let minute = 60;
			let minutes = seconds / minute;
			let decrementBy = minute / seconds;
			
			wisnet.App.setProp('secondsLeft', seconds);
			
			timerInterval = setInterval(function () {
				wisnet.App.setProp('timerRunning', true);
				secondsLeft = wisnet.App.getProp('secondsLeft');
				secondsLeft--;
				timerAmount = (secondsLeft / 60 / minutes);
				console.log(timerAmount);
				$timer.circleProgress('value', timerAmount);
				
				wisnet.App.setProp('secondsLeft', secondsLeft);
				
				$('#timer-left').text((secondsLeft + 1) + 's');
				
				if (secondsLeft <= 0) {
					clearInterval(timerInterval);
					setTimeout(function () {
						$('#timer-left').text('');
						resolve(wisnet.App.getProp('timerOriginalSeconds'));
					}, 1000);
					return;
				}
				
			}, 1000);
		});
	}
	
	function playBeeps(numberOfTimes = 1) {
		let $track;
		let track;
		
		$track = $(timerTrackSelector);
		track = $track[0];
		
		if (isNaN(track.duration)) {
			track.onloadedmetadata = function () {
				playTrack(track, numberOfTimes);
			};
		}
		else {
			playTrack(track, numberOfTimes);
		}
	}
	
	function playTrack(track, numberOfTimes) {
		let inter;
		track.currentTime = 0;
		track.volume = 1;
		track.play();
		numberOfTimes--;
		
		inter = setInterval(() => {
			if (numberOfTimes > 0) {
				track.currentTime = 0;
				track.volume = 1;
				track.play();
				numberOfTimes--;
			}
			else {
				clearInterval(inter);
			}
		}, (track.duration * 1000 / 1.5));
	}
	
});