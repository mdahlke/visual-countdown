/**
 * Created by michael on 6/4/17.
 */
const console = window.console;

const wisnet = {
		App: {
			name: 'Visual Countdown',
			version: '1.0.0',
			_debug: false,
			/**
			 * @var _property holds the properties set via the setProp() method
			 */
			_property: {
				deviceCode: null,
				cms: null,
				abstractID: null,
				locationID: null,
				updated: null,
				timezone: null,
				timezoneOffset: null
			},
			_errors: {},
			fn: {},
			widgets: {
				youtubePlaylist: {}
			},
			/**
			 * Set a property. This will overwrite a property if it currently exists
			 * @param key
			 * @param val
			 */
			setProp: function (key, val) {
				const self = this;
				if (wisnet.App._debug && this._property.hasOwnProperty(key)) {
					console.warn('Overwriting property: ' + key + '; Old Value: ' + this.getProp(key) + '; New Value: ', val);
				}
				this._property[key] = val;
			},
			/**
			 * Get a property
			 *
			 * @param key
			 * @param def Return a default value if the key is not found
			 * @returns {*}
			 */
			getProp: function (key, def = null) {
				if (this._property.hasOwnProperty(key)) {
					return this._property[key];
				}
				
				return def;
			},
			/**
			 * Retrieve an error object
			 *
			 * @param key
			 * @param def
			 */
			getError: function (key, def = null) {
				if (this._errors.hasOwnProperty(key)) {
					return this._errors[key];
				}
				
				return def;
			},
			getRoute: function (route, params) {
				let theRoute = '';
				if (this.getProp('routes').hasOwnProperty(route)) {
					theRoute = this.getProp('routes')[route];
					
					if (typeof params !== 'undefined') {
						for (const key in params) {
							theRoute = theRoute.replace('@' + key, params[key]);
						}
					}
					
					return theRoute;
				}
				
				return (typeof def !== 'undefined' ? def : null);
			},
			/**
			 * Add a function to the the app functions
			 *
			 * @param name string
			 * @param fn function
			 */
			addFn: function (name, fn) {
				if (!this.fn.hasOwnProperty(name)) {
					if (typeof fn === 'function') {
						this.fn[name] = fn;
					}
				}
			},
			/**
			 * Alias to @see addFn
			 *
			 * @param name string
			 * @param fn
			 */
			addFunction: function (name, fn) {
				this.addFn(name, fn);
			},
			/**
			 * Call a function if it exists with X # of args that are passed as an array
			 * @param name string
			 * @param args array
			 */
			call: function (name, args) {
				'use strict';
				const self = this;
				return new Promise(function (resolve, reject) {
					
					if (typeof self.fn[name] === 'function') {
						if (typeof args === 'undefined') {
							args = [];
						}
						if (self._debug) {
							console.log('Calling function: ' + name + ' with args: ', args);
						}
						self.fn[name].apply(name, args);
						
						resolve(name, args);
					}
					else if (self._debug) {
						console.error('Failed calling function: ' + name + ' with args: ', args);
						reject(name, args);
					}
				});
			},
			on: function (action, func) {
			
			},
			partial: function (file, target, placement) {
				return new Promise(function (resolve, reject) {
					wisnet.App.ajax({
						dataType: 'html',
						data: {
							action: 'partial_fetch',
							file: file
						}
					}).then(function (data, statusText, xhrObj) {
						if (typeof target === 'undefined') {
							target = 'body';
							placement = 'append';
						}
						if (typeof placement === 'undefined') {
							placement = 'overwrite';
						}
						
						if (placement === 'overwrite') {
							$(target).html(data);
						}
						else if (placement === 'append') {
							$(target).append(data);
						}
						else {
							$(target).prepend(data);
						}
						resolve(data);
					}, function (xhrObj, textStatus, err) {
						// console.log(textStatus, err);
					});
					
				});
			},
			/**
			 * Make an AJAX call
			 *  Once the call is complete, we run the parseAjaxResponse method
			 *  to check for alerts, html, etc...
			 *
			 * @param ajaxParams
			 * @param showFlashMessage
			 * @returns {Promise}
			 */
			ajax: function (ajaxParams, showFlashMessage) {
				'use strict';
				return new Promise(function (resolve, reject) {
					/**
					 * Defaults for the AJAX call
					 * @type {{type: string, url, data: {}, dataType: string}}
					 */
					const defaults = {
						type: 'get',
						url: ajax_url,
						data: {},
						dataType: 'json'
					};
					
					const params = $.extend(true, {}, defaults, ajaxParams);
					const ajaxRequest = $.ajax(params);
					
					ajaxRequest.then((response, statusText, xhrObj) => {
						wisnet.App.call('ajaxAddDebug', [params, response]);
						const deparams = wisnet.App.deparam(params.data);
						
						if (deparams.action !== 'undefined') {
							$('body').trigger('ajax_action/' + deparams.action, [response, deparams]);
						}
						
						wisnet.App.parseAjaxResponse(response, showFlashMessage);
						
						if (typeof response.data !== 'undefined' && typeof response.data.errors !== 'undefined') {
							wisnet.App.showAjaxFormErrors(response.data.errors);
						}
						
						resolve({
							status: response.status,
							response: response,
							params: deparams
						});
					}, function (xhrObj, textStatus, err) {
						wisnet.App.parseAjaxResponse(xhrObj.responseJSON);
						if (typeof wisnet.App.fn.ajaxWorking !== 'undefined') {
							wisnet.App.fn.ajaxWorking.hide();
						}
						if (typeof $.fn.popup === 'function') {
							$('#me-popup').popup('hide');
						}
						reject(xhrObj, textStatus);
					});
					
				});
			},
			/**
			 * Parse an AJAX response
			 *
			 * @since v3.0.0
			 *
			 * @param response
			 */
			parseAjaxResponse: function (response) {
				'use strict';
				
				if (typeof response !== 'object') {
					// if the response isn't a JSON object we can't do anything with it
					return;
				}
				
				if (!empty(response.triggers)) {
					$.each(response.triggers, function (i, e) {
						$('body').trigger(e.event, e.args);
					});
				}
				
				if (!empty(response.flash)) {
					
					if (response.flash.constructor !== Array) {
						response.flash = [response.flash];
					}
					$.each(response.flash, function (status, message) {
						if (typeof this.message !== 'undefined' && this.message !== '') {
							wisnet.App.fn.flash.show(this);
						}
					});
				}
				
				if (!empty(response.redirect)) {
					if (response.redirect === 'reload') {
						window.location.reload();
					}
					else {
						window.location = response.redirect;
					}
				}
				else if (!empty(response.html)) {
					const target = typeof response.target !== 'undefined' && response.target !== '' ? response.target : wisnet.App.getProp('ajaxTarget');
					const placement = typeof response.placement !== 'undefined' && response.placement !== '' ? response.placement : wisnet.App.getProp('ajaxPlacement');
					
					if (placement === 'popup') {
						wisnet.App.call('popup', [response.html]);
					}
					else if (placement === 'overwrite') {
						$(target).html(response.html);
					}
					else if (placement === 'append') {
						$(target).append(response.html);
					}
					else {
						$(target).prepend(response.html);
					}
					
				}
			},
			/**
			 *
			 * @since v3.1.1
			 */
			showAjaxFormErrors: function (errors) {
				'use strict';
				
				$('.form-group .form-error').remove();
				$('.form-group').removeClass('has-errors');
				
				if (typeof errors !== 'undefined' && errors.length) {
					$.each(errors, function (i, e) {
						let $el = $('[name="' + e.input + '"]');
						
						if ($el.length) {
							$el.closest('.form-group').addClass('has-errors').append('<div class="form-error">' + e.error + '</div>');
						}
					});
				}
				
			},
			/**
			 * Serialize a form
			 *
			 * @since v3.0.0
			 *
			 * @param formArray takes the serialized version of a form
			 *          $('form').serialize()
			 * @returns {{}}
			 */
			objectifyForm: function (formArray) {
				let returnArray = {};
				for (let i = 0; i < formArray.length; i++) {
					returnArray[formArray[i]['name']] = formArray[i]['value'];
				}
				return returnArray;
			},
			/**
			 * Takes a query string and turns it into an objects
			 *      test=1&action=test --> {test: 1, action: 'test'}
			 *
			 * @param query
			 * @returns {{}}
			 */
			deparam: function (query) {
				if (typeof query === 'undefined' || typeof query === 'object') {
					return typeof query === 'object' ? query : {};
				}
				let pairs, i, keyValuePair, key, value, map = {};
				// remove leading question mark if its there
				if (query.slice(0, 1) === '?') {
					query = query.slice(1);
				}
				if (query !== '') {
					pairs = query.split('&');
					for (i = 0; i < pairs.length; i += 1) {
						keyValuePair = pairs[i].split('=');
						key = decodeURIComponent(keyValuePair[0]);
						value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1].replace(/\+/g, '%20')) : undefined;
						map[key] = value;
					}
				}
				return map;
			},
			/**
			 * Update a URL parameter
			 * @param url
			 * @param param
			 * @param value
			 */
			updateUrlParameter: function (url, param, value) {
				const regex = new RegExp('(' + param + '=)[^\&]+');
				return url.replace(regex, '$1' + value);
			},
			/**
			 * Trigger the form action trigger
			 *
			 * This will trigger ajax_form_action/{form_action} and pass along:
			 *      response
			 *      paramse
			 *      $form
			 * @param action action name of the submitted form
			 * @param response response of the AJAX call
			 * @param params parameters that were sent in the AJAX call
			 * @param $form the form object
			 */
			triggerFormAction: function (action, response, params, $form) {
				const data = [response, params, $form];
				
				$('body').trigger('ajax_form_action/' + action, data);
			},
			triggerLinkAction: function (action, response, params, $form) {
				const data = [response, params, $form];
				$('body').trigger('ajax_link_action/' + action, data);
			},
			// local storage
			storage: function (db) {
				this.store = wisnet.App.getProp('localforage_' + db);
				
				// add a record
				this.store.set = (key, value) => {
					return this.store.setItem(key, value);
				};
				// retrieve a record
				this.store.get = (key) => {
					return this.store.getItem(key);
				};
				// get all the records
				this.store.all = () => {
					const store = this.store;
					let records = [];
					
					return new Promise(function (resolve, reject) {
						store.keys().then(function (keys) {
							store.iterate(function (value, key) {
								records[key] = value;
							}).then(function () {
								resolve(records);
								records = null;
							}).catch(function (err) {
								records = null;
							});
							
						});
					});
				};
				
				return this.store;
				
			},
		}
	}
;

const db = localforage.config({
	name: 'VisualCountdown',
	version: 1.0,
	storeName: 'timers', // Should be alphanumeric, with underscores.
	description: 'Database of timers'
});

wisnet.App.setProp('DB', db);

function empty(subject) {
	return (typeof subject === 'undefined' || subject === '' || subject === null || subject === false);
}