<?php
/**
 * File: index.php
 * Date: 3/27/18
 * Time: 10:35 PM
 *
 * @package visualcountdown.dev
 * @author Michael Dahlke <mdahlke@wisnet.com>
 */

$f3 = require_once('../vendor/bcosca/fatfree/lib/base.php');

$f3->config(__DIR__ . '/../.env');
$f3->config(__DIR__ . '/../config/config.ini');
$f3->config(__DIR__ . '/../config/routing.ini');
$f3->set('UI', __DIR__ . '/../templates/');

require('../inc/functions.php');


\Template::instance()->filter('_ternary', function ($boolean, $true, $false) {
	echo $boolean ? ($true === '??' ? $boolean : $true) : $false;
});
\Template::instance()->filter('in_array', function ($needle, array $array, $strict = false) {
	if (!is_array($array)) {
		return false;
	}

	return in_array($needle, $array, $strict);
});

$f3->run();