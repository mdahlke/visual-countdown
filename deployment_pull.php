<?php

require ('deploy/config.php');

// Pre pull
if (file_exists('deploy/pre_pull.php')) {
    require('deploy/pre_pull.php');
}

// Pull
if (file_exists('deploy/repo_pull.php')) {
    require('deploy/repo_pull.php');
}
// Post pull
if (file_exists('deploy/post_pull.php')) {
    require('deploy/post_pull.php');
}