const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');

const order = require('gulp-order');
const jshint = require('gulp-jshint');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');

const jsFiles = 'src/js/*.js';
const jsDestination = './public/assets/js/';
const vendorJS = 'src/js/vendor/*.js';
const vendorJSDestination = './public/assets/js/';
const appJS = 'src/js/app/*.js';
const appJSDestination = './public/assets/js/';
const scssFiles = 'src/scss/**/*.scss';
const scssDestination = './public/assets/css/';

gulp.task('styles', function () {
	return gulp.src(scssFiles)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(scssDestination));
});

gulp.task('lint', function () {
	return gulp.src(jsFiles)
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('scripts', function () {
	return gulp.src(jsFiles)
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(order([
			'circle-progress.js',
			'main.js'
		], {base: './src/js/'}))
		.pipe(concat('site.js'))
		.pipe(gulp.dest(jsDestination))
		.pipe(rename('site.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(jsDestination));
});

gulp.task('vendor', function () {
	return gulp.src(vendorJS)
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(order([
			'jquery.min.js',
			'jquery.touch-punch.min.js',
			'bootstrap.min.js',
			'localforage.js',
			'spectrum.js'
		], {base: './src/js/vendor/'}))
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest(vendorJSDestination))
		.pipe(rename('vendor.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(vendorJSDestination));
});

gulp.task('app', function () {
	return gulp.src(appJS)
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(concat('app.js'))
		.pipe(gulp.dest(appJSDestination))
		.pipe(rename('app.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(appJSDestination));
});

//Watch task
gulp.task('watch', function () {
	gulp.watch(scssFiles, ['styles']);
	gulp.watch(jsFiles, ['scripts']);
	gulp.watch(vendorJS, ['vendor']);
	gulp.watch(appJS, ['vendor']);
});

gulp.task('default', ['styles', 'scripts', 'vendor', 'app', 'watch']);