<?php
/**
 * Created by PhpStorm.
 * @author Michael Dahlke <mdahlke@wisnet.com>
 * Date: 8/26/17
 * Time: 8:29 AM
 */

namespace Flash;


class Flash {
	private static $_messages = [];

	public static function add($message, $status = 'danger') {
		$messages = self::$_messages;

		$messages[] = ['message' => $message, 'status' => $status];

		self::$_messages = $messages;
	}

	public static function getMessages() {
		return self::$_messages;
	}

	public static function stringify() {
		$messages = '';

		foreach (self::$_messages as $message) {
			//            p($message);
			$message .= '
                <div class="flash-message ' . $message['status'] . '">
                    <span class="flash - close"></span>
                    <div class="flash - content">
                        ' . $message['message'] . '
                    </div>
                </div>
            ';
		}

		self::clearMessages();
		return $messages;
	}

	public static function clearMessages() {
		self::$_messages = [];
	}
}