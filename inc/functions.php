<?php
/**
 * File: functions.php
 * Date: 3/28/18
 * Time: 7:41 PM
 *
 * @package visualcountdown.dev
 * @author Michael Dahlke <mdahlke@wisnet.com>
 */


function isAJAXRequest() {
	return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
}

function isFormRequest($type = null) {
	$request = false;
	if (!empty($_REQUEST)) {
		// request is defined
		if (count($_REQUEST) > 1) {
			// request has more than one entry - this is a form request
			switch ($type) {
				case 'POST':
					$request = count($_POST) > 0 ? true : false;
					break;
				case 'GET':
					$request = count($_GET) > 0 ? true : false;
					break;
				default:
					$request = true;
			}
		}
		else {
			// only one entry might be IIS added parameter
			foreach ($_REQUEST as $key => $value) {
				if ($key !== 'PHPSESSID' && !empty($value)) {
					$request = true;
				}
			}
		}
	}
	if (!$request && !empty($_FILES)) {
		$request = true;
	}
	return $request;
}

/**
 * Custom var_dump
 *
 * @param mixed takes N args
 */
function v() {
	if (function_exists('xdebug_get_code_coverage')) {
		foreach (func_get_args() as $arg) {
			var_dump($arg);
		}
	}
	else {
		foreach (func_get_args() as $arg) {
			echo '<pre>';
			var_dump($arg);
			echo '</pre>';
		}
	}

}

/**
 * Custom print_r
 *
 * @param mixed takes N args
 */
function p() {
	foreach (func_get_args() as $arg) {
		echo '<pre>';
		print_r($arg);
		echo '</pre>';
	}
}

/**
 * Quick debug output
 *
 * @param mixed takes N args
 */
function d() {
	foreach (func_get_args() as $arg) {
		echo '<pre>';
		echo($arg);
		echo '</pre>';
	}
}