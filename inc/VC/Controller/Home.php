<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 8/16/17
 * Time: 9:50 PM
 */

namespace VC\Controller;

use AssetManager\AssetManager;
use VC\Service\Authorization;

class Home extends Base {

	/**
	 * Show the timer
	 *
	 * @param \Base $f3
	 * @param array $params
	 *
	 * @return bool
	 */
	public function indexAction(\Base $f3, $params) {
		$minutes = [];
		$seconds = [];
		for ($i = 0; $i <= 10; $i++) {
			$minutes[] = $i;
		}
		for ($i = 0; $i <= 50; $i += 1) {
			$seconds[] = $i;
		}

		$f3->set('minutes', $minutes);
		$f3->set('seconds', $seconds);
		$f3->set('defaultMinute', 1);
		$f3->set('defaultSecond', 0);

		return $this->template('home/index.html');
	}

}