<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 8/17/17
 * Time: 7:55 AM
 */


namespace VC\Controller;

use AssetManager\AssetManager;
use Flash\Flash;
use VC\Model\Alert;
use VC\Model\Debug;

class Base {
	const
		FILE_TYPE_JSON = 'json',
		FILE_TYPE_HTML = 'html';
	/**
	 * @var \Base $f3
	 */
	public static $f3;
	/** @var string */
	protected static $requestedFileType = 'html';

	/** @var bool */
	private static $pageRendered = false;
	/** @var bool */
	private static $pageRedirected = false;
	/** @var Users|null */
	protected static $user;
	/** @var string */
	protected $viewTemplate = 'home/index.php';
	protected $pageTitle = 'Display Editor';

	public function __construct() {

		if (self::$f3 === null) {
			self::$f3 = \Base::instance();

			self::$f3->set('basePath', __DIR__ . '/../..');


			$routes = [];
			$i = 0;
			foreach (self::$f3->ROUTES as $pattern => $route) {
				$name = $i;
				foreach ($route as $key => $val) {
					$name = (isset($val['GET']) ? $val['GET'][3] : (isset($val['POST']) ? $val['POST'][3] : $i));
				}
				$routes[$name] = $pattern;
				$i++;
			}

			self::$f3->set('routes', $routes);
		}

		self::$f3->set('Controller', $this);

	}

	/**
	 * @param             $content string
	 * @param string|null $view
	 * @param array $passThrough
	 * @param bool|true $showAlerts
	 *
	 * @return bool
	 */
	protected function renderHTML($content = null, $view = null, $passThrough = [], $showAlerts = true, $template = false) {
		// HTML request

		foreach ($passThrough as $key => $value) {
			${$key} = $value;
		}

		// if this view is called via ajax or from within a page - render without wrapper
		if ($view === null && $content === null) {
			$view = 'common/error.php';
		}

		self::$f3->set('jsFiles', AssetManager::formatJsFiles('footer'));
		self::$f3->set('cssFiles', AssetManager::formatCssFiles('footer'));
		self::$f3->set('cssStyles', AssetManager::formatCssStyle());
		self::$f3->set('jsFooterScripts', AssetManager::getJsScripts(true));
		self::$f3->set('jsFilesHead', AssetManager::formatJsFiles('head'));
		self::$f3->set('cssFilesHead', AssetManager::formatCssFiles('head'));
		self::$f3->set('hasDebug', false);

		if (self::$f3->get('SESSION.debug')) {

			$debug = [
				'Latest Commit' => Debug::getLatestCommit(),
				'User/Organization' => [
					'User ID' => (self::$f3->exists('User') ? self::$f3->get('User')->ID : ''),
					'User Name' => (self::$f3->exists('User') ? self::$f3->get('User')->First : '') . ' ' . (self::$f3->exists('User') ? self::$f3->get('User')->Last : ''),
					'Admin Level' => (self::$f3->exists('User') ? self::$f3->get('User')->Admin_Level : ''),
					'Organization Title' => (self::$f3->exists('Organization') ? self::$f3->get('Organization')->Title : ''),
				],
				'Basic' => [
					'PATH' => self::$f3->PATH,
					'BASE' => self::$f3->BASE,
					'ALIAS' => self::$f3->ALIAS,
				],
				'Params' => self::$f3->get('PARAMS'),
				'SESSION' => self::$f3->get('SESSION'),
				'POST' => self::$f3->get('POST'),
				'GET' => self::$f3->get('GET'),
			];

			$debug += Debug::getDebug();
			self::$f3->set('hasDebug', $debug);
		}

		if (self::$f3->exists('SESSION.alerts')) {
			self::$f3->set('doAlerts', self::$f3->get('SESSION.alerts'));
			self::$f3->clear('SESSION.alerts');
		}

		if (isAJAXRequest() || self::$pageRendered) {
			if ($view !== null) {
				if ($template) {
					return \Template::instance()->render($view);
				}

				return \View::instance()->render($view);
			}
			else {
				return ($content);
			}
		}
		else {
			if ($template) {
				echo \Template::instance()->render($view);
			}
			else {
				echo \View::instance()->render($view);
			}

		}

		return true;

	}

	protected function renderJSON($passThrough = [], $errorCode = 200) {
		// json request
		$allowResponse = true;
		if ($allowResponse) {
			send_json($passThrough, $errorCode);
		}

	}


	/**
	 * Render a response as either html or json objects
	 *
	 * @param       $view string
	 * @param array $passThrough
	 * @param bool $showAlerts
	 *
	 * @param bool $template
	 * @param int $errorCode
	 * @return bool
	 */
	protected function render($view = null, $passThrough = [], $showAlerts = true, $template = false, $errorCode = 200) {
		// do not render view if page has already been redirected;
		if (self::$pageRedirected) {
			return false;
		}
		elseif (self::$requestedFileType == self::FILE_TYPE_JSON) {
			$json = $passThrough;
			$json['flash'] = Flash::getMessages();
			$json['html'] = ($view ? $this->renderHTML(null, $view, $passThrough, $showAlerts, $template, $errorCode) : false);

			$this->renderJSON($json, $errorCode);
		}
		else {
			$this->renderHTML(null, $view, $passThrough, $showAlerts, $template, $errorCode);
		}

		return true;
	}

	/**
	 * Render a template via the render method
	 *
	 * @param null $view
	 * @param array $passthrough
	 * @param bool $showAlerts
	 *
	 * @return bool
	 */
	protected function template($view = null, $passthrough = [], $showAlerts = true) {
		return $this->render($view, $passthrough, $showAlerts, true);
	}

	public function beforeRoute() {
		self::$requestedFileType = isset(self::$f3->get('PARAMS')['fileType']) ? self::$f3->get('PARAMS')['fileType'] : self::FILE_TYPE_HTML;

		if (isset($_SERVER["HTTP_ACCEPT"]) && strpos($_SERVER["HTTP_ACCEPT"], 'json')) {
			self::$requestedFileType = self::FILE_TYPE_JSON;
		}
	}

	/**
	 * Redirect to a new page.
	 *
	 * @param $route string
	 *
	 * @return bool
	 */
	protected function redirect($route) {
		if (self::$pageRedirected) {
			return;
		}
		self::$pageRedirected = true;
		if (!isset($_SERVER["HTTP_ACCEPT"]) || strpos($_SERVER["HTTP_ACCEPT"], 'json') === false) {
			header('Location: ' . $route, true, 302);
		}
		else {
			// json redirect
			$passThrough = ['redirect' => $route];
			if (!empty($alerts)) {
				$passThrough['alerts'] = $alerts;
			}
			echo json_encode($passThrough);

		}

		return true;
	}

	public function ajaxPartialFetch() {
		$partial = self::$f3->get('GET.file');

		$this->render($partial);
	}

	public function setAlert($alert, $status = 'success') {
		if (self::$f3->exists('SESSION.alerts')) {
			$alerts = self::$f3->get('SESSION.alerts');
		}
		else {
			$alerts = [];
		}
		$alerts[] = new Alert($alert, $status);
		self::$f3->set('SESSION.alerts', $alerts);
	}

}