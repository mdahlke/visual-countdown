<?php
/**
 * Created by PhpStorm.
 * User: mdahlke
 * Date: 2/9/18
 * Time: 7:50 AM
 */

namespace VC\Controller;
use Flash\Flash;


/**
 * Class ActionStatus
 * @package Markey
 *
 * @property object|null $class;
 * @property boolean $status;
 * @property string $message;
 * @property array $errors;
 */
class ActionStatus {
	public $class = null;
	public $status = false;
	public $message = '';
	public $errors = [];

	/**
	 * ActionStatus constructor.
	 *
	 * @param bool $status
	 * @param string $message
	 * @param array $errors
	 * @param null $class
	 */
	public function __construct($status, $message, $errors = [], $class = null) {
		$this->status = filter_var($status, FILTER_VALIDATE_BOOLEAN);
		$this->message = filter_var($message, FILTER_SANITIZE_STRING);
		$this->errors = $errors;
		$this->class = $class;
	}

	public function addToFlash() {
		if(!empty($this->message)) {
			Flash::add($this->message, ($this->status ? 'success' : 'danger'));
		}
		return $this;
	}

}