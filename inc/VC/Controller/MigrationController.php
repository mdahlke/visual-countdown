<?php
/**
 * Created by PhpStorm.
 * User: mdahlke
 * Date: 2/2/18
 * Time: 2:09 PM
 */

namespace VC\Controller;


use VC\Model\MigrationFile;

class MigrationController {

	public function migrate(\Base $f3, $params) {
		if (php_sapi_name() != "cli") {
			echo 'You cannot do this' . PHP_EOL;
			return false;
		}
		$filesProcessed = [];
		$filesFailed = [];
		$filesSkipped = [];
		$isDryRun = $f3->exists('GET.dry-run') || $f3->exists('GET.d');

		$migration = new MigrationFile();
		foreach (scandir(ADMIN_BASE_PATH . '/../migration') as $filename) {
			$file = ADMIN_BASE_PATH . '/../migration/' . $filename;


			if (is_file($file)) {


				if (strpos($filename, '--skip') !== false) {
					$filesSkipped[] = $filename;
				}
				else {
					$processed = $migration->processFile($filename, $isDryRun);

					if ($processed) {
						$filesProcessed[] = $filename;
					}
					else {
						$filesFailed[] = $filename;
					}
				}
			}
		}

		if ($isDryRun) {
			echo 'DRY RUN. NO FILES WERE PROCESSED.' . PHP_EOL . PHP_EOL;
		}

		echo "Files processed:" . PHP_EOL;
		echo implode(PHP_EOL, $filesProcessed);
		echo PHP_EOL . PHP_EOL;
		echo "Files NOT processed:" . PHP_EOL;
		echo implode(PHP_EOL, $filesFailed);
		echo PHP_EOL . PHP_EOL;
		echo "Files SKIPPED:" . PHP_EOL;
		echo implode(PHP_EOL, $filesSkipped);
		echo PHP_EOL . PHP_EOL;

		if ($isDryRun) {
			echo 'DRY RUN. NO FILES WERE PROCESSED.' . PHP_EOL . PHP_EOL;
		}

	}

}