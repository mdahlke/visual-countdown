<?php
/**
 * File: Form.php
 * Date: 3/23/18
 * Time: 9:49 PM
 *
 * @package markeyds.dev
 * @author Michael Dahlke <mdahlke@wisnet.com>
 */

namespace VC\Service;


class FormField {
	/**
	 * @var
	 *
	 * @return
	 */
	public $label;
	/**
	 * @var
	 *
	 * @return
	 */
	public $description;
	/**
	 * @var
	 *
	 * @return
	 */
	public $name;
	/**
	 * @var bool
	 *
	 * @return bool
	 */
	public $required = false;
	/**
	 * @var string
	 *
	 * @return string
	 */
	public $requiredText = '*';
	/**
	 * @var
	 *
	 * @return
	 */
	public $value;
	/**
	 * @var
	 *
	 * @return
	 */
	public $inputType;
	/**
	 * @var
	 *
	 * @return
	 */
	public $subFields;
	/**
	 * @var
	 *
	 * @return
	 */
	public $selectedValueText;
	/**
	 * @var bool
	 *
	 * @return bool
	 */
	public $toggled = false;

	/**
	 * 'label' => 'Middle Name',
	 * 'description' => '',
	 * 'name' => 'Middle',
	 * 'required' => true,
	 * 'requiredText' => '*',
	 * 'value' => $this->user->Middle,
	 * 'inputType' => 'text',
	 * 'selectedValueText' => false,
	 * 'toggled' => false,
	 */

	public function __construct(array $fields = null) {
		// parent::__construct();

		if ($fields) {
			foreach ($fields as $key => $val) {
				$this->$key = $val;
			}
		}
	}

	/**
	 * @return mixed
	 */
	public function getLabel() {
		return $this->label;
	}

	/**
	 * @param mixed $label
	 * @return FormField
	 */
	public function setLabel($label) {
		$this->label = $label;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param mixed $description
	 * @return FormField
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 * @return FormField
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isRequired() {
		return $this->required;
	}

	/**
	 * @param bool $required
	 * @return FormField
	 */
	public function setRequired($required) {
		$this->required = $required;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getRequiredText() {
		return $this->requiredText;
	}

	/**
	 * @param string $requiredText
	 * @return FormField
	 */
	public function setRequiredText($requiredText) {
		$this->requiredText = $requiredText;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * @param mixed $value
	 * @return FormField
	 */
	public function setValue($value) {
		$this->value = $value;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getInputType() {
		return $this->inputType;
	}

	/**
	 * @param mixed $inputType
	 * @return FormField
	 */
	public function setInputType($inputType) {
		$this->inputType = $inputType;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSubFields() {
		return $this->subFields;
	}

	/**
	 * @param mixed $subFields
	 * @return FormField
	 */
	public function setSubFields($subFields) {
		$this->subFields = $subFields;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSelectedValueText() {
		return $this->selectedValueText;
		return $this;
	}

	/**
	 * @param mixed $selectedValueText
	 * @return FormField
	 */
	public function setSelectedValueText($selectedValueText) {
		$this->selectedValueText = $selectedValueText;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isToggled() {
		return $this->toggled;
	}

	/**
	 * @param bool $toggled
	 * @return FormField
	 */
	public function setToggled($toggled) {
		$this->toggled = $toggled;
		return $this;
	}

}