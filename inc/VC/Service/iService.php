<?php
/**
 * Created by PhpStorm.
 * User: mdahlke
 * Date: 2/12/18
 * Time: 12:33 PM
 */

namespace VC\Service;


interface iService {

	public function validate();

	public function sanitize();

	public function process();

}