<?php

namespace VC\Template;

use AssetManager\AssetManager;

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 9/14/17
 * Time: 3:21 PM
 */
class Form extends \Prefab {
	private $f3 = null;

	public function __construct() {
		if (!$this->f3) {
			$this->f3 = \Base::instance();
		}
	}

	public function smart_input($obj) {
		$input = ('input_' . str_replace('input_', '', $obj->inputType));

		return $this->{$input}($obj);
	}

	public function form_start($form) {
		$this->f3->set('formObject', $form);

		return \Template::instance()->render('form/form_start.html');
	}

	public function form_end($form) {
		$html = '';
		$html .= isset($form['id']) ? '<input type="hidden" name="id" value="' . $form['id'] . '"">' : '';
		$html .= '<button class="btn btn-primary" type="submit" name="' . isset($form['name']) . '" value="submit">Submit</button>';
		$html .= '</form>';

		return $html;
	}

	public function input_text($obj) {
		$obj->extras = !isset($obj->extras) ? [] : $obj->extras;

		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_text.html');
	}

	public function input_textarea($obj) {
		$obj->textEditorType = !isset($obj->textEditorType) ? 'basic' : $obj->textEditorType;

		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_textarea.html');
	}

	public function input_ckeditor($obj) {
		$obj->textEditorType = 'ckeditor';

		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_textarea.html');
	}

	public function input_dollar($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_currency.html');
	}

	public function input_number($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_number.html');
	}

	public function input_slider($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_slider.html');
	}

	public function input_select($obj) {
		$this->f3->set('fieldObject', $obj);

		if (is_array($obj->value)) {
			return \Template::instance()->render('form/input_select_multiple.html');
		}

		return \Template::instance()->render('form/input_select.html');
	}

	public function input_checkbox($obj) {
		$this->f3->set('fieldObject', $obj);

		if (!isset($obj->pretty) || $obj->pretty === false || $obj->pretty) {
			return \Template::instance()->render('form/input_checkbox_pretty.html');
		}
		return \Template::instance()->render('form/input_checkbox.html');
	}

	public function input_radio($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_radio.html');
	}

	public function input_date($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_date.html');
	}

	public function input_time($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_time.html');
	}

	public function input_password($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_password.html');
	}

	public function input_hidden($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_hidden.html');
	}

	public function input_media($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_media.html');
	}

	public function input_color($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_color.html');
	}

	public function input_spectrum($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_spectrum.html');
	}

	public function input_hexcolor($obj) {
		return $this->input_color($obj);
	}

	public function input_url($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/input_url.html');
	}

	public function toggle_form_group($obj) {
		$this->f3->set('fieldObject', $obj);

		return \Template::instance()->render('form/toggle-form-group.html');
	}

}
