<?php
/**
 * Created by PhpStorm.
 * User: briankolstad
 * Date: 11/3/16
 * Time: 2:42 PM
 */

namespace VC\Model;

use \DB\SQL\Mapper;

/**
 * Class MigrationFile
 * @package RegistrationApp
 * @property $migrationId integer
 * @property $fileName string
 * @property $dateExecuted string
 */
class MigrationFile extends Mapper {

	public function __construct() {
		/** @var \PDO $DB */
		$DB = \Base::instance()->get('DB');
		try {
			parent::__construct($DB, 'migration_file');
		} catch (\Exception $e) {
			$DB->exec('CREATE TABLE migration_file (
                      migrationId int(11) NOT NULL AUTO_INCREMENT,
                      fileName varchar(255),
                      dateExecuted datetime,
                      PRIMARY KEY (migrationId),
                      KEY file(fileName)
                    )ENGINE=InnoDB DEFAULT CHARSET=utf8');
			parent::__construct($DB, 'migration_file');
		}
	}

	/**
	 * Process a migration file
	 * @param string $fileName
	 * @param int $dryRun
	 */
	public function processFile($fileName, $dryRun = true) {
		$this->load(['fileName = ?', $fileName]);

		/** @var \PDO $DB */
		if ($this->dry()) {

			if($dryRun){
				// this would be ran if it weren't a dry run
				return true;
			}

			$DB = \Base::instance()->get('DB');
			/** @noinspection PhpIncludeInspection */
			require_once ADMIN_BASE_PATH . '/../migration/' . $fileName;
			$this->fileName = $fileName;
			$this->dateExecuted = date("Y-m-d H:i:s");
			return $this->save();
		}
	}
}