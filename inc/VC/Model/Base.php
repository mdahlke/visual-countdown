<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 8/28/17
 * Time: 1:04 PM
 */

namespace VC\Model;

class Base extends AuthorizationMapper {
	/**
	 * @var string Corresponding database table name
	 */
	protected $_table;
	protected $container;
	protected $prefix = '';
	protected $_checkboxInputs = [];
	/**
	 * Fields that should never be updated (Case Sensitive)
	 *
	 * @var array
	 */
	protected static $_doNotUpdate = ['ID', 'Created_At'];

	public function __construct($fields = null, $ttl = null) {
		if ($this->_table) {
			parent::__construct(\Base::instance()->get('DB'), $this->_table, $fields, $ttl);
		}

		$this->container = self::$f3->get('container');
	}

	public function getPrefix() {
		return $this->prefix;
	}

	public function loadBy($key, $value, $options = null) {
		$filter = [$key . '= ?', $value];

		$this->load($filter, $options);
	}

	public function loadByID($id, $options = []) {
		return $this->loadBy('ID', $id, $options);
	}

	public function findBy($key, $value, $options = []) {
		$filter = [$key . '= ?', $value];

		$options = array_merge_recursive(['limit' => 1], $options);

		return $this->find($filter, $options);
	}

	public function findByID($id, $options = []) {
		return $this->findBy('ID', $id, $options);
	}

	public function isUpdateable($field) {
		return !in_array($field, self::$_doNotUpdate) ? true : false;
	}

	public static function getProtected($field) {
		return self::$$field;
	}

	public function getCheckboxInputs() {
		return $this->_checkboxInputs;
	}
}