<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 8/25/17
 * Time: 9:54 AM
 */

namespace VC\Model;


class Alert {
    public $message = '';
    public $status = '';

    public function __construct($message, $status = 'success') {
        $this->message = $message;
        $this->status = $status;

        $f3 = \Base::instance();
        $alerts = $f3->exists('SESSION.alerts') ? $f3->get('SESSION.alerts') : [];

        $alerts[] = (object)[
            'status' => $status,
            'message' => $message,
        ];

        $f3->set('SESSION.alerts', $alerts);
    }
}