<?php

/**
 * Created by PhpStorm.
 * User: briankolstad
 * Date: 2/1/17
 * Time: 2:30 PM
 */

namespace VC\Model;

use \DB\SQL\Mapper;

class AuthorizationMapper extends Mapper {
	/**
	 * @var static \Base $f3
	 */
	protected static $f3;

	public function __construct($db, $table, $fields = null, $ttl = null) {
		parent::__construct($db, $table, $fields, $ttl);

		if (self::$f3 === null) {
			self::$f3 = \Base::instance();
		}

		$this->beforesave(function ($self, $pkeys) {
			/** @var $self AuthorizationMapper */
			if ($self->canSave()) {
				//                if(in_array('Updated',$self->fields(false)))
				//                    $self['Updated']=$self->getDateTimeFormat();
				return true;
			}
			else {
				return false;
			}
		});
		$this->beforeupdate(function ($self, $pkeys) {
			/** @var $self AuthorizationMapper */
			return $self->canUpdate();
		});
		$this->beforeinsert(function ($self, $pkeys) {
			/** @var $self AuthorizationMapper */
			if ($self->canInsert()) {
				if (in_array('Created', $self->fields(false))) {
					$self['Created'] = $self->getDateTimeFormat();
				}
				return true;
			}
			else {
				return false;
			}
		});
		$this->beforeerase(function ($self, $pkeys) {
			/** @var $self AuthorizationMapper */
			if ($self->canErase()) {
				if (in_array('Deleted', $self->fields(false))) {
					$self['Deleted'] = $self->getDateTimeFormat();
				}
				return true;
			}
			else {
				return false;
			}
		});
	}

	/**
	 * @param string[] $exclude
	 * @return mixed
	 */
	public function duplicate($exclude = []) {
		$result = new $this();

		foreach ($this->fields() as $key) {
			if (!in_array($key, $exclude)) {
				$result[$key] = $this[$key];
			}
		}
		return $result;
	}

	/**
	 * @return bool
	 */
	public function canEdit() {
		return $this->canSave();
	}

	/**
	 * @return bool
	 */
	public function canSave() {
		return true;
	}

	/**
	 * @return bool
	 */
	public function canUpdate() {
		return $this->canSave();
	}

	/**
	 * @return bool
	 */
	public function canInsert() {
		return $this->canSave();
	}

	/**
	 * @return bool
	 */
	public function canErase() {
		return $this->canSave();
	}

	public function getDateTimeFormat($dateTime = null) {
		if ($dateTime === null) {
			$dateTime = time();
		}
		return date('Y-m-d H:i:s', $dateTime);
	}

	public function dump() {
		$v = ['name' => get_class($this), 'data' => []];
		foreach ($this->fields() as $field) {
			$v['data'][$field] = $this[$field];
		}
		print_r($v);
	}

}