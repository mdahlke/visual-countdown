<?php

namespace AssetManager;

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 7/7/17
 * Time: 1:26 PM
 */

class AssetManager {
	private static $jsFiles = ['head' => [], 'footer' => []];
	private static $cssFiles = ['head' => [], 'footer' => []];
	private static $jsScripts = [];
	private static $cssStyle = [];

	/**
	 * @return array
	 */
	public static function getJsFiles() {
		if (empty(self::$jsFiles)) {
			self::$jsFiles = [
				'head' => [],
				'footer' => [],
			];
		}

		return self::$jsFiles;
	}

	public static function formatJsFiles($location = 'head') {
		$files = self::getJsFiles();

		$f3 = \Base::instance();
		$output = '';
		foreach ($files[$location] as $handle => $attrs) {
			//				$base = $f3->get('CDN_URL');
			//			if($f3->get('MARKEY_SCREEN') === $f3->get('MARKEY_SCREEN_ADMIN')){
			//			$base = $f3->get('ADMIN_URL');
			//			}
			$output .= '<script id="' . $handle . '" src="' . ($f3->get('MARKEY_SCREEN') === $f3->get('MARKEY_SCREEN_DISPLAY') ? $f3->get('CDN_URL') : '') . $attrs['src'] . '"></script>';
		}

		return $output;
	}

	public static function outputJsFiles($location = 'head') {
		echo self::formatJsFiles($location);
	}

	/**
	 * @param array $jsFiles
	 */
	private static function setJsFiles($jsFiles) {
		self::$jsFiles = $jsFiles;
	}

	public static function addJsFile($handle, $src, $dependencies = '', $inFooter = false) {
		$jsFiles = self::getJsFiles();
		$location = $inFooter ? 'footer' : 'head';

		$jsFiles[$location][$handle] = [
			'src' => $src,
			'dependencies' => $dependencies,
		];

		self::setJsFiles($jsFiles);
	}

	/**
	 * @return array
	 */
	public static function getCssFiles() {
		return self::$cssFiles;
	}

	public static function formatCssFiles($location = 'head') {
		$files = self::getCssFiles();
		$f3 = \Base::instance();
		$output = '';
		foreach ($files[$location] as $handle => $attrs) {
			$output .= '<link id="' . $handle . '" href="' . ($f3->get('MARKEY_SCREEN') === $f3->get('MARKEY_SCREEN_DISPLAY') ? $f3->get('CDN_URL') : '') . $attrs['src'] . '" rel="stylesheet"/>';
		}

		return $output;
	}

	public static function outputCssFiles($location = 'head') {
		echo self::formatCssFiles($location);
	}

	/**
	 * @param array $cssFiles
	 */
	public static function setCssFiles($cssFiles) {
		self::$cssFiles = $cssFiles;
	}

	public static function addCssFile($handle, $src, $dependencies = '', $inFooter = false) {
		$cssFiles = self::getCssFiles();
		$location = $inFooter ? 'footer' : 'head';

		$cssFiles[$location][$handle] = [
			'src' => $src,
			'dependencies' => $dependencies,
		];

		self::setCssFiles($cssFiles);
	}


	/**
	 * @return array|string
	 */
	public static function getJsScripts($implode = false) {
		return $implode ? implode('; ', self::$jsScripts) : self::$jsScripts;
	}

	/**
	 * @param array $jsScripts
	 */
	public static function setJsScripts($jsScripts) {
		self::$jsScripts = $jsScripts;
	}

	public static function addJsScript($script) {
		$scripts = self::getJsScripts();

		$scripts[] = $script;

		self::setJsScripts($scripts);
	}

	/**
	 * @return array
	 */
	public static function getCssStyle() {
		return self::$cssStyle;
	}

	/**
	 * @return array
	 */
	public static function formatCssStyle() {
		return '<style>' . implode("\n", self::$cssStyle) . '</style>';
	}

	public static function addCssStyle($style) {
		$styles = self::getCssStyle();

		$styles[] = $style;

		self::setCssStyle($styles);
	}

	/**
	 * @param array $cssStyle
	 */
	public static function setCssStyle($cssStyle) {
		self::$cssStyle = $cssStyle;
	}

	public static function dump() {
		p(self::$cssFiles);
		p(self::$cssStyle);
		p(self::$jsFiles);
		p(self::$jsScripts);
	}

}